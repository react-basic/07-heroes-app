import { mount } from "enzyme"
import { MemoryRouter } from "react-router-dom";
import { AuthContext } from "../../../auth/authContext";
import { Navbar } from "../../../components/ui/Navbar";
import { types } from "../../../types/types";

const mockNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => mockNavigate,
}));


describe('Pruebas en <Navbar />', () => {


    test('debe de mostrarse correctamente', () => {

        const initialValue = {
            user : {
                logged: true,
                name: 'Pedro'
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={initialValue}>
                <MemoryRouter initialEntries={['/']} >
                    <Navbar />
                </MemoryRouter>
            </AuthContext.Provider>
        );

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('.text-info').text().trim()).toBe('Pedro');

    })

    test('debe de llamar el logout, llamar el navigate y el dispatch con los argumentos', () => {

        const initialValue = {
            dispatch: jest.fn(),
            user : {
                logged: true,
                name: 'Pedro'
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={initialValue}>
                <MemoryRouter initialEntries={['/']} >
                    <Navbar />
                </MemoryRouter>
            </AuthContext.Provider>
        );

        wrapper.find('button').prop('onClick') ();

        expect(initialValue.dispatch).toHaveBeenCalledWith({'type': types.logout});
        expect(mockNavigate).toHaveBeenCalledWith('/login', {
            replace: true
        });


    })

})