import { heroes } from "../data/heroes"


export const getHeroByPublisher = (publisher) => {

    console.log('getHeroByPublisher called');

    const validPublishers = ['DC Comics', 'Marvel Comics'];

    if (!validPublishers.includes(publisher)) {
        throw new Error(`Invalid publisher: ${publisher}`);
    }

    return heroes.filter(hero => hero.publisher === publisher)
}