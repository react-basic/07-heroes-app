import { mount } from "enzyme";
import { MemoryRouter } from "react-router-dom";
import { AuthContext } from "../../../auth/authContext";
import { LoginScreen } from "../../../components/login/LoginScreen";
import { types } from "../../../types/types";

const mockNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => mockNavigate,
}));


describe('Pruebas en <LoginScreen />', () => {

    const initialValue = {
        dispatch: jest.fn(),
        user: {
            name: 'Christian',
            logged: false
        }
    }

    const wrapper = mount(
        <AuthContext.Provider value={initialValue}> 
            <MemoryRouter initialEntries={['/login']} >
                <LoginScreen />
            </MemoryRouter>
        </AuthContext.Provider>
    );

    test('debe hacer match con el snapshot', () => {

        expect(wrapper).toMatchSnapshot();

    })


    test('debe de realizar el dispatch y la navegación', () => {

        wrapper.find('button').simulate('click');

        expect(initialValue.dispatch).toHaveBeenCalledWith({
            type: types.login,
            payload: {
                name: 'Christian',
            }
        })
        expect(mockNavigate).toHaveBeenCalledWith('/marvel', {replace: true});

        localStorage.setItem('lastPath', '/dc');
        wrapper.find('button').simulate('click');

        expect(mockNavigate).toHaveBeenCalledWith('/dc', {replace: true});

    })

})